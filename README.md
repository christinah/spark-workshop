Spark Workshop SfS
=======

## Getting started
For the examples in this tutorial, it will be sufficient to use Spark in interactive mode. If you are interested in an IDE/building standalone applications, see the references below. 

### Requirements
Java 6 or 7 (is probably already installed on your machine)

If you wish to use Python, you will also need a Python interpreter (2.6 or newer, not Python 3) and numpy for the machine learning library MLlib.

In the tutorial and the exercises, I will mainly use the Scala API to Spark.

### Download

Download Spark from <http://spark.apache.org/downloads.html>, choosing the latest version and for the package type select "Pre-built for Hadoop 2.4 and later". After having downloaded and unpacked the .tgz file, cd into the folder and open the interactive Scala Shell with

```bash
./bin/spark-shell
```

Try the following command, which should return 1000:

```bash
scala> sc.parallelize(1 to 1000).count()
```

If you prefer Python, you can use the Python shell:

    ./bin/pyspark
    
And run the following command, which should also return 1000:

    >>> sc.parallelize(range(1000)).count()


## Further References

These references are meant for those who intend to delve deeper into Spark, they are not required for the tutorial.

### IDE
See Dropbox "setup" folder (link sent by email).

### Scala Tutorial
For a concise Scala tutorial, see <http://ampcamp.berkeley.edu/5/exercises/introduction-to-the-scala-shell.html>. We will shortly go through this at the beginning of the hands-on session.
Note that you can use the spark-shell for this if you do not want to install sbt.

More details are provided in this tutorial <https://twitter.github.io/scala_school/>. 

### Spark 
- http://spark.apache.org/docs/latest/quick-start.html
- http://ampcamp.berkeley.edu/5/exercises/index.html