from operator import add
data = sc.textFile("README.md")
wc = data
      .flatMap(lambda x: x.split(' '))
      .map(lambda x: (x, 1))
      .reduceByKey(add)
wc.saveAsTextFile("wc_out")