val data = sc.textFile("README.md")
val wc = data
		  .flatMap(l => l.split(" "))
		  .map(word => (word, 1))
		  .reduceByKey(_ + _)
wc.saveAsTextFile("wc_out")