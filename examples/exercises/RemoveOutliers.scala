/**
 * Illustrates remove outliers in Scala using summary Stats
 */

import org.apache.spark.rdd.RDD

def removeOutliers(rdd: RDD[Double]): RDD[Double] = {
  val summaryStats = rdd.stats()
  val stddev = math.sqrt(summaryStats.variance)
  // your code goes here: filter the input rdd so that it only 
  // contains elements which are smaller than 3*stddev after centerting
  // use math.abs() if needed
}

val input = sc.parallelize(List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1000)).map(_.toDouble)
val result = removeOutliers(input)
println(result.collect().mkString(","))