Exercises
====


## 1) Warm up

To warm up, I suggest you go through the steps described here: 
<http://ampcamp.berkeley.edu/5/exercises/data-exploration-using-spark.html> 

The folder ``pagecounts`` with the data files can be found in the `data` folder in the repository.

When loading the data file with

```scala
val pagecounts = sc.textFile("../data/pagecounts")
```

make sure to set the path relative to where you put the Spark folder.

## 2) Exercises 
Afterwards, you can look at the .scala files in this folder and fill in the missing code snippets. 

- BasicMapThenFilter.scala
- RemoveOutliers.scala
- Regression.scala


## Useful links
- Introduction to the Scala shell: <http://ampcamp.berkeley.edu/5/exercises/introduction-to-the-scala-shell.html>
- Scala anonymous function syntax: <http://docs.scala-lang.org/tutorials/tour/anonymous-function-syntax.html>
- Spark quick start: <http://spark.apache.org/docs/latest/quick-start.html>
- Spark programming guide: <http://spark.apache.org/docs/latest/programming-guide.html>
- MLlib programming guide: <http://spark.apache.org/docs/1.2.1/mllib-guide.html>
