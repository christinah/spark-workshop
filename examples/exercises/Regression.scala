import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors

// Load and parse the data
val data = sc.textFile("data/mllib/ridge-data/lpsa.data")
val parsedData = data.map { line =>
  val parts = line.split(',')
  LabeledPoint(parts(0).toDouble, Vectors.dense(parts(1).split(' ').map(_.toDouble)))
}.cache()

// Building the model
val numIterations = 100
val model = LinearRegressionWithSGD.train(parsedData, numIterations)

// Evaluate model on training examples and compute training error
val valuesAndPreds = parsedData.map { point =>
  val prediction = model.predict(point.features)
  (point.label, prediction)
}

val MSE = // your code goes here: compute the MSE from valuesAndPreds
println("training Mean Squared Error = " + MSE)

// Then, browse the Spark API (http://spark.apache.org/docs/1.2.1/api/scala/index.html#org.apache.spark.mllib.regression.package => scroll down to value members)
// to change the algorithm to do 1) ridge regression, 2) the Lasso 
// adjust the step size, the mini batch fraction and the regulization parameter (where applicable) 
// don't forget the necessary imports

// Further extensions:
// you could first center the data as described here: http://spark.apache.org/docs/1.2.1/mllib-feature-extraction.html
// split the data into training and test set using the randomSplit function (http://spark.apache.org/docs/1.2.1/api/scala/index.html#org.apache.spark.rdd.RDD)
