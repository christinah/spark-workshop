import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors

// Load and parse the data
val data = sc.textFile("data/mllib/ridge-data/lpsa.data")
val parsedData = data.map { line =>
  val parts = line.split(',')
  LabeledPoint(parts(0).toDouble, Vectors.dense(parts(1).split(' ').map(_.toDouble)))
}.cache()

// Building the model
val numIterations = 100
val model = LinearRegressionWithSGD.train(parsedData, numIterations)

// Evaluate model on training examples and compute training error
val valuesAndPreds = parsedData.map { point =>
  val prediction = model.predict(point.features)
  (point.label, prediction)
}

val MSE = valuesAndPreds.map{case(v, p) => math.pow((v - p), 2)}.mean()
println("training Mean Squared Error = " + MSE)


// Then, browse the Spark API (http://spark.apache.org/docs/1.2.1/api/scala/index.html#org.apache.spark.mllib.regression.package => scroll down to value members)
// to change the algorithm to do 1) ridge regression, 2) the Lasso 
// adjust the step size, the mini batch fraction and the regulization parameter (where applicable) 
// don't forget the necessary imports

// parameters
val stepSize = 0.1
val miniBatchFraction = 0.5
val lambda = 0.1

// Lasso
import org.apache.spark.mllib.regression.LassoWithSGD
val model = LassoWithSGD.train(parsedData, numIterations, stepSize, lambda, miniBatchFraction)

// Ridge
import org.apache.spark.mllib.regression.RidgeRegressionWithSGD
val model = RidgeRegressionWithSGD.train(parsedData, numIterations, stepSize, lambda, miniBatchFraction)


// Further extensions:
// you could first center the data as described here: http://spark.apache.org/docs/1.2.1/mllib-feature-extraction.html
import org.apache.spark.mllib.feature.StandardScaler
import org.apache.spark.RDD.rdd

// one possibility is to define a function taking the options whether to scala and/or center the features and/or the response
def centerAndScale(data : RDD[LabeledPoint], centerFeatures : Boolean, centerResponse : Boolean, scaleFeatures : Boolean, scaleResponse : Boolean) : RDD[LabeledPoint] = {

	// create StandardScaler for features
	val scaler_features =
	  if(centerFeatures || scaleFeatures)
	    new StandardScaler(withMean = centerFeatures, withStd = scaleFeatures).fit(data.map(x => x.features))
	  else
	    null

	// create StandardScaler for response
	val scaler_response =
	  if(centerResponse || scaleResponse)
	    new StandardScaler(withMean = centerResponse, withStd = scaleResponse).fit(data.map(x => Vectors.dense(x.label)))
	  else
	    null


	if(centerFeatures || scaleFeatures || centerResponse || scaleResponse)
	  // apply to data
	  data.map(x => LabeledPoint(
	    if(centerResponse || scaleResponse) scaler_response.transform(Vectors.dense(x.label))(0) else x.label,
	    if(centerFeatures || scaleFeatures) scaler_features.transform(x.features) else x.features
	  ))
	else
	  data
}

  // center and/or scale
val centerFeatures = true
val centerResponse = true
val scaleFeatures = true
val scaleResponse = false
val dataCenteredAndScaled = centerAndScale(parsedData, centerFeatures, centerResponse, scaleFeatures, scaleResponse)

// split the data into training and test set using the randomSplit function (http://spark.apache.org/docs/1.2.1/api/scala/index.html#org.apache.spark.rdd.RDD)
val proportionTest = 0.8
val seed = 1
val splits = parsedData.randomSplit(Array(1 - proportionTest, proportionTest), seed)
val trainingData = splits(0)
val testData = splits(1)