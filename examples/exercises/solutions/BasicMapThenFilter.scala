/**
 * Illustrates a simple map the filter in Scala
 */


val input = sc.parallelize(List(1,2,3,4))
val squared = input.map(x => x*x)
val result = squared.filter(x => x != 1)
println(result.collect().mkString(","))
